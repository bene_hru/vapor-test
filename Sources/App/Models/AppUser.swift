import HTTP
import Fluent
import Turnstile
import TurnstileCrypto
import TurnstileWeb
import Auth

final class AppUser: User {
    // Field for the Fluent ORM
    var exists: Bool = false

    // Database Fields
    var id: Node?
    var username: String
    var password = ""
    var apiKeyID = URandom().secureToken
    var apiKeySecret = URandom().secureToken

    /**
     Authenticates a set of credentials against the User.
     */
    static func authenticate(credentials: Credentials) throws -> User {
        var user: AppUser?

        switch credentials {
                /**
                 Fetches a user, and checks that the password is present, and matches.
                 */
        case let credentials as UsernamePassword:
            let fetchedUser = try AppUser.query()
                    .filter("username", credentials.username)
                    .first()
            if let password = fetchedUser?.password,
               password != "",
               (try? BCrypt.verify(password: credentials.password, matchesHash: password)) == true {
                user = fetchedUser
            }

                /**
                 Fetches the user by session ID. Used by the Vapor session manager.
                 */
        case let credentials as Identifier:
            user = try AppUser.find(credentials.id)

                /**
                 Authenticates via API Keys
                 */
        case let credentials as APIKey:
            user = try AppUser.query()
                    .filter("api_key_id", credentials.id)
                    .filter("api_key_secret", credentials.secret)
                    .first()

        default:
            throw UnsupportedCredentialsError()
        }

        if let user = user {
            return user
        } else {
            throw IncorrectCredentialsError()
        }
    }

    /**
     Registers users for UsernamePassword.
     */
    static func register(credentials: Credentials) throws -> User {
        var newUser = AppUser(credentials: credentials as! UsernamePassword)

        if try AppUser.query().filter("username", newUser.username).first() == nil {
            try newUser.save()
            return newUser
        } else {
            throw AccountTakenError()
        }
    }

    init(credentials: UsernamePassword) {
        self.username = credentials.username
        self.password = BCrypt.hash(password: credentials.password)
    }

    /**
     Initializer for Fluent
     */
    init(node: Node, in context: Context) throws {
        id = node["id"]
        username = try node.extract("username")
        password = try node.extract("password")
        apiKeyID = try node.extract("api_key_id")
        apiKeySecret = try node.extract("api_key_secret")
    }

    /**
     Serializer for Fluent
     */
    func makeNode(context: Context) throws -> Node {
        return try Node(node: [
                "id": id,
                "username": username,
                "password": password,
                "api_key_id": apiKeyID,
                "api_key_secret": apiKeySecret
        ])
    }

    static func prepare(_ database: Database) throws {
    }

    static func revert(_ database: Database) throws {
    }

}

extension Request {
    func user() throws -> AppUser {
        guard let user = try auth.user() as? AppUser else {
            throw "Invalid user type"
        }
        return user
    }
}

extension String: Error {
}
