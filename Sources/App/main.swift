import Vapor
import Auth
import HTTP
import Cookies
import Turnstile
import TurnstileCrypto
import TurnstileWeb
import Fluent
import Foundation

let drop = Droplet()

drop.database = Database(MemoryDriver())
drop.middleware.append(AuthMiddleware(user: AppUser.self))
drop.middleware.append(TrustProxyMiddleware())
drop.preparations.append(AppUser.self)

let controller = MainController()
controller.addRoutes(drop: drop)

//drop.get("/name",":name") { request in
//    if let name = request.parameters["name"]?.string {
//        return "Hello \(name)!"
//    }
//    return "Error retrieving parameters."
//}


/**
 Endpoint for the home page.
 */
//drop.get { request in
//    let user = try? request.user()
//
//    var dashboardView = try Node(node: [
//            "authenticated": user != nil,
//            "baseURL": request.baseURL
//    ])
//    dashboardView["account"] = try user?.makeNode()
//
//    return try drop.view.make("index", dashboardView)
//}

/**
 Login Endpoint
 */
drop.get("login") { request in
    return try drop.view.make("login")
}

drop.post("login") { request in
    guard let username = request.formURLEncoded?["username"]?.string,
          let password = request.formURLEncoded?["password"]?.string else {
        return try drop.view.make("login", ["flash": "Missing username or password"])
    }
    let credentials = UsernamePassword(username: username, password: password)
    do {
        try request.auth.login(credentials)
        return Response(redirect: "/")
    } catch let e {
        return try drop.view.make("login", ["flash": "Invalid username or password"])
    }
}

/**
 Registration Endpoint
 */
drop.get("register") { request in
    return try drop.view.make("register")
}

drop.post("register") { request in
    guard let username = request.formURLEncoded?["username"]?.string,
          let password = request.formURLEncoded?["password"]?.string else {
        return try drop.view.make("register", ["flash": "Missing username or password"])
    }
    let credentials = UsernamePassword(username: username, password: password)

    do {
        try _ = AppUser.register(credentials: credentials)
        try request.auth.login(credentials)
        return Response(redirect: "/")
    } catch let e as TurnstileError {
        return try drop.view.make("register", Node(node: ["flash": e.description]))
    }
}

/**
 API Endpoint for /me
 */
let protect = ProtectMiddleware(error: Abort.custom(status: .unauthorized, message: "Unauthorized"))

drop.grouped(BasicAuthenticationMiddleware(), protect).group("api") { api in
    api.get("me") { request in
        return try JSON(node: request.user().makeNode())
    }
}

/**
 Logout endpoint
 */
drop.post("logout") { request in
    request.subject.logout()
    return Response(redirect: "/")
}


drop.run()
