//
// Created by Benedikt Hruschka on 12.02.17.
//

import Vapor

final class NavigationItem: NodeRepresentable {

    var id: Node?
    var exists: Bool = false

    var title: String
    var destination: String
    var active: Bool

    init(title: String, destination: String, active: Bool = false) {
        self.id = nil
        self.title = title
        self.destination = destination
        self.active = active
    }

    init(node: Node, in context: Context) throws {
        id = try node.extract("id")
        title = try node.extract("title")
        destination = try node.extract("destination")
        active = try node.extract("active")
    }

    func makeNode(context: Context) throws -> Node {
        return try Node(node: [
                "id": id,
                "title": title,
                "destination": destination,
                "active": active
        ])
    }

//    static func prepare(_ database: Database) throws {
//        try database.create("acronyms") { users in
//            users.id()
//            users.string("short")
//            users.string("long")
//        }
//    }
//
//    static func revert(_ database: Database) throws {
//        try database.delete("acronyms")
//    }

}

