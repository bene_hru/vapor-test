import Vapor
import HTTP

final class MainController {

    let endpoints: [(destination: String, title: String, view: String)] = [
            ("/", "About", "welcome"),
            ("download", "Download", "download"),
            ("upload", "Upload", "upload"),
            ("contact", "Contact", "contact")

    ]

    func addRoutes(drop: Droplet) {
        endpoints.forEach { endpoint in
            drop.get(endpoint.destination) { request in
                let user = try? request.user()
                var infos = try Node(node: [
                        "authenticated": user != nil,
                        "name": "Leaf 🍃",
                        "navigationItems": try self.navigationItems(for: endpoint.destination).makeNode()
                ])
                infos["account"] = try user?.makeNode()
                print("account: \(try user?.makeNode())")

                return try drop.view.make(endpoint.view, infos)
            }
        }
    }

    fileprivate func navigationItems(for destination: String) -> [NavigationItem] {
        var navigationItems: [NavigationItem] = []
        endpoints.forEach { element in
            navigationItems.append(NavigationItem(title: element.title, destination: element.destination, active: element.destination == destination))
        }
        return navigationItems
    }


}